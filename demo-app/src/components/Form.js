import React from "react";
import moment from "moment";
import * as antd from "antd";
const { TextArea } = antd.Input;

const labelSpan = 12;
const inputSpan = 24;

class Form extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      brand: props.item ? props.item.brand : "",
      model: props.item ? props.item.model : "",
      notes: props.item ? props.item.notes : "",
      price: props.item ? props.item.price : "",
      dateAcquired: props.item ? moment(props.item.dateAcquired) : moment()
    };
  }

  handleOnChangeBrand = e => {
    const brand = e.target.value;
    this.setState(() => ({
      brand: brand
    }));
  };

  handleOnChangeModel = e => {
    const model = e.target.value;
    this.setState(() => ({
      model: model
    }));
  };

  handleOnChangeNotes = e => {
    const notes = e.target.value;
    this.setState(() => ({
      notes: notes
    }));
  };

  handleOnChangePrice = e => {
    const price = e.target.value;
    if (price.match(/^\d{1,}(\.{0,1}\d{0,2})?$/)) {
      this.setState(() => ({
        price: price
      }));
    }
  };

  handleOnDateChange = dateAcquired => {
    this.setState(() => ({
      dateAcquired: dateAcquired
    }));
  };

  handleOnSubmit = e => {
    e.preventDefault();

    if (!this.state.brand || !this.state.model || !this.state.price) {
      antd.message.error("Error");
    } else {
      this.props.handleOnSubmit({
        brand: this.state.brand,
        model: this.state.model,
        notes: this.state.notes,
        price: parseFloat(this.state.price, 10),
        dateAcquired: this.state.dateAcquired.valueOf()
      });
    }
  };

  render() {
    return (
      <div className="component-container">
        {this.props.mode === "add" ? (
          <h2 className="p__mode">CREATE</h2>
        ) : (
          <h2 className="p__mode">EDIT</h2>
        )}
        <div className="form-container">
          <antd.Form layout="horizontal" onSubmit={this.handleOnSubmit}>
            <antd.Row>
              <antd.Col span={labelSpan}>
                <p className="form_label">Brand</p>
              </antd.Col>
              <antd.Col span={inputSpan}>
                <antd.Input
                  prefix={<antd.Icon type="shopping" />}
                  placeholder="Brand"
                  allowClear={true}
                  onChange={this.handleOnChangeBrand}
                  value={this.state.brand}
                />
              </antd.Col>
            </antd.Row>
            <antd.Row>
              <antd.Col span={labelSpan}>
                <p className="form_label">Model</p>
              </antd.Col>
              <antd.Col span={inputSpan}>
                <antd.Input
                  prefix={<antd.Icon type="clock-circle" />}
                  placeholder="Model"
                  allowClear={true}
                  onChange={this.handleOnChangeModel}
                  value={this.state.model}
                />
              </antd.Col>
            </antd.Row>
            <antd.Row>
              <antd.Col span={labelSpan}>
                <p className="form_label">Notes</p>
              </antd.Col>
              <antd.Col span={inputSpan}>
                <TextArea
                  onChange={this.handleOnChangeNotes}
                  value={this.state.notes}
                  rows={2}
                  placeholder="Notes"
                />
              </antd.Col>
            </antd.Row>
            <antd.Row>
              <antd.Col span={labelSpan}>
                <p className="form_label">Price</p>
              </antd.Col>
              <antd.Col span={inputSpan}>
                <antd.Input
                  className="ant-price"
                  addonBefore="$"
                  placeholder="price"
                  value={this.state.price}
                  onChange={this.handleOnChangePrice}
                />
              </antd.Col>
            </antd.Row>
            <antd.Row>
              <antd.Col span={labelSpan}>
                <p className="form_label">Acquisition Date</p>
              </antd.Col>
              <antd.Col span={inputSpan}>
                <antd.DatePicker
                  className="form__ant-date"
                  value={this.state.dateAcquired}
                  onChange={this.handleOnDateChange}
                  format={"MMMM Do, YYYY"}
                />
              </antd.Col>
            </antd.Row>
            <antd.Button
              htmlType="submit"
              type="primary"
              className="form_submit"
            >
              {this.props.mode === "add" ? "Submit" : "Update"}
            </antd.Button>
          </antd.Form>
        </div>
      </div>
    );
  }
}

export default Form;
