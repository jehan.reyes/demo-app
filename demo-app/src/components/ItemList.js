import React from "react";
import { connect } from "react-redux";
import Item from "./Item";
import selector from "../store/selector";
import sumSelector from "../store/sumSelector";
import * as antd from "antd";
import { startRemoveItem } from "../actions/items";

const ItemList = props => (
  <div>
    <antd.Row className="item-list-label-row">
      <antd.Col span={4} offset={10}>
        <h2 className="list-div__title">ITEM LIST</h2>
      </antd.Col>
    </antd.Row>

    <antd.Row>
      <antd.Col span={16} offset={4}>
        <div className="itemList">
          {props.items.map(item => {
            return (
              <Item
                key={item.id}
                items={item}
                handleRemove={() => {
                  props.dispatch(startRemoveItem({ id: item.id }));
                }}
              />
            );
          })}
        </div>
      </antd.Col>
    </antd.Row>

    <antd.Row className="item-list-footer">
      <antd.Col span={4} offset={1}>
        <p>
          Viewing{" "}
          <span className="item_count">
            ({sumSelector(props.items).total}){" "}
          </span>
          item/s
        </p>
      </antd.Col>
      <antd.Col span={4} offset={14}>
        <p>
          Total Cost -{" "}
          <span className="item_price">
            $ {sumSelector(props.items).sum.toFixed(2)}
          </span>
        </p>
      </antd.Col>
    </antd.Row>
  </div>
);

const mapStateToProps = state => {
  return {
    items: selector(state.items, state.filters)
  };
};

export default connect(mapStateToProps)(ItemList);
