import React from "react";
import { Link } from "react-router-dom";
import moment from "moment";
import * as antd from "antd";

const { Meta } = antd.Card;

class Item extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      modalVisible: false
    };
  }

  handleClickRemove = e => {
    this.setState({
      modalVisible: true
    });
  };

  handleOnOk = () => {
    this.props.handleRemove();

    this.setState({
      modalVisible: false
    });
  };

  handleOnCancel = () => {
    this.setState({
      modalVisible: false
    });
  };

  render() {
    return (
      <div className="item">
        <antd.Popover
          title={`${this.props.items.brand} ${this.props.items.model}`}
          content={moment(this.props.items.dateAcquired).format("MMM D, YYYY")}
        >
          <antd.Card
            className="item-card"
            hoverable
            cover={
              <img
                alt="example"
                src="https://image.shutterstock.com/image-vector/vector-cartoon-classic-mens-wrist-450w-615428642.jpg"
              />
            }
            actions={[
              <Link to={`/edit/${this.props.items.id}`}>
                <antd.Icon className="anticon-edit" type="edit" />
              </Link>,
              <antd.Icon
                className="anticon-delete"
                onClick={this.handleClickRemove}
                type="delete"
              />
            ]}
          >
            <Meta
              className="list-meta"
              title={`${this.props.items.brand} ${this.props.items.model}`}
              description={`$ ${this.props.items.price.toFixed(2)}`}
            />
          </antd.Card>
        </antd.Popover>

        <antd.Modal
          title="Confirm Delete"
          centered
          visible={this.state.modalVisible}
          onOk={this.handleOnOk}
          onCancel={this.handleOnCancel}
        >
          <p>Are you sure you want to delete the item?</p>
        </antd.Modal>
      </div>
    );
  }
}

export default Item;
