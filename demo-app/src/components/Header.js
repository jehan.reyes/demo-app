import React from "react";
import { NavLink } from "react-router-dom";
import * as antd from "antd";

const Header = () => (
  <header className="header">
    <div className="header-container-div">
      <antd.Row className="title-row">
        <antd.Col span={12} offset={6}>
          <h1 className="header__title">&lt; / &gt;</h1>
        </antd.Col>
      </antd.Row>
      <antd.Row className="navigation__row">
        <antd.Col span={12} className="navigation__col">
          <NavLink to="/" activeClassName="is-active" exact>
            DASHBOARD
          </NavLink>
        </antd.Col>
        <antd.Col span={12} className="navigation__col">
          <NavLink to="/create" activeClassName="is-active">
            ADD ITEM
          </NavLink>
        </antd.Col>
      </antd.Row>
    </div>
  </header>
);

export default Header;
