import React from "react";
import Form from "./Form";
import { connect } from "react-redux";
import { startAddItem } from "../actions/items";

const Create = props => (
  <div className="create-div">
    <div className="create-form-div">
      <Form
        handleOnSubmit={item => {
          props.dispatch(startAddItem(item));
          props.history.push("/");
        }}
        mode="add"
      />
    </div>
  </div>
);

export default connect()(Create);
