import React from "react";
import { Link } from "react-router-dom";

const PageNotFound = () => (
  <div className="container__not-found">
    <div className="div__not-found">
      <h1 className="h1__404">404</h1>
      <h2 className="h2__404">This doesn't seem to be a valid page...</h2>
      <Link to="/">
        <p className="p__404">Click Here to Go Home</p>
      </Link>
    </div>
  </div>
);

export default PageNotFound;
