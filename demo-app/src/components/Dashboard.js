import React from "react";
import ItemList from "./ItemList";
import ItemFilters from "./ItemFilters";

const Dashboard = () => (
  <div className="dashboard-container">
    <div className="dashboard-content">
      <div className="dashboard-filters-div">
        <ItemFilters />
      </div>
      <div className="dashboard-itemlist-div">
        <ItemList />
      </div>
    </div>
  </div>
);

export default Dashboard;
