import React from "react";
import { connect } from "react-redux";
import {
  setTextFilter,
  sortByDate,
  sortByPrice,
  setOrderAscending,
  setOrderDescending
} from "../actions/filters";
import * as antd from "antd";
const Option = antd.Select.Option;

const ItemTextFilters = props => (
  <div className="item-filters-div">
    <antd.Row className="ant-row-top">
      <antd.Col span={10} offset={7}>
        <antd.Input
          prefix={<antd.Icon type="search" />}
          placeholder="Text Filter"
          allowClear={true}
          value={props.filters.text}
          onChange={e => {
            props.dispatch(setTextFilter(e.target.value));
          }}
        />
      </antd.Col>
    </antd.Row>
    <antd.Row className="ant-row-bot">
      <antd.Col span={4} offset={9}>
        <antd.Select
          className="itemfitlers__select"
          defaultValue="date"
          onChange={value => {
            value === "date"
              ? props.dispatch(sortByDate())
              : props.dispatch(sortByPrice());
          }}
        >
          <Option value="date">Date</Option>
          <Option value="price">Price</Option>
        </antd.Select>
      </antd.Col>
      <antd.Col span={1} className="order-icon">
        <antd.Icon
          className="anticon-filter"
          type="up-circle"
          onClick={() => {
            props.dispatch(setOrderAscending());
          }}
        />
      </antd.Col>
      <antd.Col span={1} className="order-icon">
        <antd.Icon
          className="anticon-filter"
          type="down-circle"
          onClick={() => {
            props.dispatch(setOrderDescending());
          }}
        />
      </antd.Col>
    </antd.Row>
  </div>
);

const mapStateToProps = state => {
  return {
    filters: state.filters
  };
};

export default connect(mapStateToProps)(ItemTextFilters);
