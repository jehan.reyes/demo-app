const { override, fixBabelImports, addLessLoader } = require("customize-cra");

module.exports = override(
  fixBabelImports("import", {
    libraryName: "antd",
    libraryDirector: "es",
    style: true
  }),
  addLessLoader({
    javascriptEnabled: true,
    modifyVars: {}
  })
);
